#!/usr/bin/python3

import io
import math
import argparse


TWEET_MAX_SIZE = 280


# initialise argument parser
parser = argparse.ArgumentParser(
    description=(
        "Convert input text into smaller tweets of 280 chars prefixed with "
        "the current tweet index/total count of tweets"
    )
)

# define the expected arguments as CLI input while running this program
parser.add_argument(
    'text',
    type=str,
    help="Long text which needs to be convereted into tweets"
)

# parse the known arguments and ignore unknown arguments
args, _ = parser.parse_known_args()
text = args.text


def is_power_of_10(number):
    """
    returns True if given number is a power of 10 otherwise False
    """
    return math.log10(number) % 1 == 0


def get_prefix(index, count):
    """
    returns prefix part of the tweet as per given index and count
    """
    return "{index}/{count} ".format(index=index, count=count)


def get_tweet_size(index):
    """
    calculate and return the size of each tweet by calculating the
    length of the prefix for each tweet using index and count.

    Note: length of prefix increases by 2 for every 10**n; n >= 0;
    """
    if index == 1:
        return TWEET_MAX_SIZE
    elif index == 2:
        return TWEET_MAX_SIZE - 2 * len(get_prefix(index, index))
    else:
        return (
            TWEET_MAX_SIZE -
            len(get_prefix(index, index)) -
            (index-1 if is_power_of_10(index) else 0)
        )


def get_tweet_count(text):
    """
    returns the number of tweets required to post the whole input text content
    """
    remaining_text_length = len(text)
    count = 0
    while remaining_text_length > 0:
        count += 1
        remaining_text_length -= get_tweet_size(count)
    return count


def main(text):
    """
    This is a main function of the program where the execution flow starts.
    It fetches the list of tweets from `get_tweets` function and print them
    with attaching the appropriate prefix to the tweets.
    """

    # get count of tweets from the given text
    count = get_tweet_count(text)

    if count == 1:
        print(text)
    else:
        text_stream = io.StringIO(text)
        for index in range(1, count+1):

            prefix = get_prefix(index, count)
            content = text_stream.read(TWEET_MAX_SIZE - len(prefix))
            print(prefix + content)


if __name__ == '__main__':
    main(text)
