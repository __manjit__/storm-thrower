import unittest

from storm import (
    get_tweet,
    get_tweets,
    get_split_index,
    get_prefixed_tweets,
)


class TweetStormTestCase(unittest.TestCase):
    """
    TweetStormTestCase to run tests on storm.py
    """

    def setUp(self):
        """
        setUp method is to setup test data required for executing
        the test cases
        """
        self.tweet_char_max_limit = 270
        self.max_tweet_size_with_prefix = 280
        self.text_0 = "This is a sample text to be convereted into a tweet."
        self.text_1 = (
            "This is your last chance. After this, there is no turning back. "
            "You take the blue pill—the story ends, you wake up in your bed "
            "and believe whatever you want to believe. You take the red "
            "pill—you stay in Wonderland, and I show you ? how deep the rabbit "
            "hole goes! Remember: all I'm offering is the truth. Nothing more."
        )
        self.text_2 = (
            "What is Lorem Ipsum? Lorem Ipsum is simply dummy text of the "
            "printing and typesetting industry. Lorem Ipsum has been the "
            "industry's standard dummy text ever since the 1500s, when an "
            "unknown printer took a galley of type and scrambled it to make "
            "a type specimen book. It has survived not only five centuries, "
            "but also the leap into electronic typesetting, remaining "
            "essentially unchanged. Can you believe this? It was popularised "
            "in the 1960s with the release of Letraset sheets containing "
            "Lorem Ipsum passages, and more recently with desktop publishing "
            "software like Aldus PageMaker including versions of Lorem Ipsum. "
            "Why do we use it?"
        )
        self.text_3 = (
            "WhatisLoremIpsum?LoremIpsumissimplydummytextoftheprintingandtype"
            "settingindustry.LoremIpsumhasbeentheindustry'sstandarddummytexte"
            "versincethe1500s,whenanunknownprintertookagalleyoftypeandscrambl"
            "edittomakeatypespecimenbook.!thassurvivednotonlyfivecenturies,bu"
            "talsotheleapintoelectronictypesetting,remainingessentiallyunchan"
            "ged.Itwaspopularisedinthe1960swiththereleaseofLetrasetsheetscont"
            "ainingLoremIpsumpassages,andmorerecentlyw!thdesktoppublishingsof"
            "twarelikeAldusPageMakerincludingversionsofLoremIpsum.Whydoweuseit?"
        )
        self.no_space = ""
        self.space_terminator = " "
        self.exclamation_mark_terminator = "!"
        self.question_mark_terminator = "?"

    def test_get_split_index_for_no_space(self):
        raw_tweet = self.text_1[:self.tweet_char_max_limit]
        self.assertEqual(271, get_split_index(raw_tweet, self.no_space))

        raw_tweet = self.text_2[:self.tweet_char_max_limit]
        self.assertEqual(271, get_split_index(raw_tweet, self.no_space))

        raw_tweet = self.text_3[:self.tweet_char_max_limit]
        self.assertEqual(271, get_split_index(raw_tweet, self.no_space))

    def test_get_split_index_for_space(self):
        raw_tweet = self.text_1[:self.tweet_char_max_limit]
        self.assertEqual(263, get_split_index(
            raw_tweet, self.space_terminator))

        raw_tweet = self.text_2[:self.tweet_char_max_limit]
        self.assertEqual(270, get_split_index(
            raw_tweet, self.space_terminator))

        raw_tweet = self.text_3[:self.tweet_char_max_limit]
        self.assertEqual(0, get_split_index(
            raw_tweet, self.space_terminator))

    def test_get_split_index_for_exclamation_mark_terminator(self):
        raw_tweet = self.text_1[:self.tweet_char_max_limit]
        self.assertEqual(262, get_split_index(
            raw_tweet, self.exclamation_mark_terminator))

        raw_tweet = self.text_2[:self.tweet_char_max_limit]
        self.assertEqual(0, get_split_index(
            raw_tweet, self.exclamation_mark_terminator))

        raw_tweet = self.text_2[:self.tweet_char_max_limit]
        self.assertEqual(0, get_split_index(
            raw_tweet, self.exclamation_mark_terminator))

    def test_get_split_index_for_question_mark_terminator(self):
        raw_tweet = self.text_1[:self.tweet_char_max_limit]
        self.assertEqual(
            231, get_split_index(raw_tweet, self.question_mark_terminator))

    def test_get_tweet(self):
        raw_tweet = self.text_1[:self.tweet_char_max_limit]
        expected_tweet = raw_tweet[:263]
        tweet = get_tweet(raw_tweet)
        self.assertEqual(expected_tweet, tweet)

    def test_get_tweets(self):
        text = self.text_2 * 10
        tweets = get_tweets(text)
        self.assertEqual(24, len(tweets))

    def test_get_tweets_for_length_of_each_tweet(self):
        text = self.text_2 * 10
        tweets = get_tweets(text)
        self.assertTrue(
            all([self.tweet_char_max_limit >= len(tweet) for tweet in tweets])
        )

        text = self.text_2 * 1000
        tweets = get_tweets(text)
        self.assertTrue(
            all([self.tweet_char_max_limit >= len(tweet) for tweet in tweets])
        )

        text = self.text_3 * 100
        tweets = get_tweets(text)
        self.assertTrue(
            all([self.tweet_char_max_limit >= len(tweet) for tweet in tweets])
        )

    def test_get_prefixed_tweets(self):
        tweets = [self.text_0]
        total_count = len(tweets)

        expected_tweets = [
            "{tweet}".format(tweet=tweet)
            for index, tweet in enumerate(tweets, 1)
        ]
        self.assertEqual(expected_tweets, get_prefixed_tweets(tweets))

        tweets = [self.text_0] * 125
        total_count = len(tweets)

        expected_tweets = [
            "{}/{} {}".format(index, total_count, tweet)
            for index, tweet in enumerate(tweets, 1)
        ]
        self.assertEqual(expected_tweets, get_prefixed_tweets(tweets))

        tweets = [self.text_2] * 249
        total_count = len(tweets)

        expected_tweets = [
            "{}/{} {}".format(index, total_count, tweet)
            for index, tweet in enumerate(tweets, 1)
        ]
        self.assertEqual(expected_tweets, get_prefixed_tweets(tweets))

        tweets = [self.text_3] * 343
        total_count = len(tweets)

        expected_tweets = [
            "{}/{} {}".format(index, total_count, tweet)
            for index, tweet in enumerate(tweets, 1)
        ]
        self.assertEqual(expected_tweets, get_prefixed_tweets(tweets))
