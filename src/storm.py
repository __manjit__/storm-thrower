#!/usr/bin/python3

import argparse

TWEET_CHAR_MAX_LIMIT = 270


# initialise argument parser
parser = argparse.ArgumentParser(description='Convert text into tweets')

# define the expected arguments as CLI input while running this program
parser.add_argument(
    'text', type=str,
    help='Long text which needs to be convereted into tweets')

# parse the known arguments and ignore unknown arguments
args, _ = parser.parse_known_args()
text = args.text


def get_split_index(raw_tweet, terminator):
    """
    returns the position where tweet should split based on given terminator
    character. i.e ' ', '?', '!'
    """
    # find a terminator character in the raw tweet extracted from the text
    return raw_tweet.rfind(terminator) + 1


def get_tweet(raw_tweet):
    """
    """
    # find the split index to split the tweet extracted from input text
    end = (
        get_split_index(raw_tweet, ' ') or
        get_split_index(raw_tweet, '!') or
        get_split_index(raw_tweet, '?')
    )

    tweet = raw_tweet[0:end]
    return tweet


def get_tweets(text):
    """
    returns the list of tweets within TWEET_CHAR_MAX_LIMIT
    """
    # initialize with empty list to keep the tweets
    tweets = []

    # loop through the text while text is `gte` TWEET_CHAR_MAX_LIMIT
    # take the slice from the text which can fit as a tweet and add to tweets
    # update the text with remaining part of the text for further processing

    while len(text) >= TWEET_CHAR_MAX_LIMIT:

        # get a slice of text as a raw tweet
        raw_tweet = text[0:TWEET_CHAR_MAX_LIMIT]

        # get splitted tweet in limit of TWEET_CHAR_MAX_LIMIT
        tweet = get_tweet(raw_tweet)

        # update text with remaining part
        text = text[len(tweet):]

        # append tweet to a list to return on completion of text processing
        tweets.append(tweet)

    # when the length of text is `lt` TWEET_CHAR_MAX_LIMIT,
    # simply add the text into tweets and return
    tweets.append(text)

    # return list of splitted tweets under given size limit
    return tweets


def get_prefixed_tweets(tweets):
    """
    returns tweets from the given list after attaching an appropriate prefix
    """
    # initialize prefixed tweets list
    prefixed_tweets = []

    # total count of tweets in the formed list
    total_tweets_count = len(tweets)

    # if total_tweets_count is equal to 1; then no need to add prefix
    if total_tweets_count == 1:
        return tweets

    # append tweets with appropriate prefix
    for index, tweet in enumerate(tweets, 1):
        if tweet.startswith(' '):
            prefixed_tweet = (
                "{0}/{1}{2}".format(index, total_tweets_count, tweet)
            )
        else:
            prefixed_tweet = (
                "{0}/{1} {2}".format(index, total_tweets_count, tweet)
            )

        prefixed_tweets.append(prefixed_tweet)
    return prefixed_tweets


def main(text):
    """
    This is a main function of the program where the execution flow starts.
    It fetches the list of tweets from `get_tweets` function and print them
    with attaching the appropriate prefix to the tweets.
    """
    tweets = get_tweets(text)
    prefixed_tweets = get_prefixed_tweets(tweets)

    for tweet in prefixed_tweets:
        print(tweet)


if __name__ == '__main__':
    main(text)
