# Storm is here!

## Important source code files in this repository are following

  * storm.py - contains an implemention of tweetstrom
  * test_storm.py - contains test cases for functions in storm.py
  * ty_storm.py - contains an implemention of tweetstrom with a different approach


## Important Notes

  * Both `storm.py` and `ty_storm.py` are written to run on python3 and assumes that python executable is available on path `/usr/bin/python3`.

  * Both `storm.py` and `ty_storm.py` have executable permission given with `chmod +x <filename>` and should run fine as `/path/filename "input_text"` as long as the python3 path matches the shebang `#!/usr/bin/python3` added in the top of the file.

  * Both `storm.py` and `ty_storm.py` expects the input to be a single text string which is pretty much like every other unix command input, extra arguments are ignored.

  * `storm.py` splits the tweets pre-maturely to avoid word splits but works fine for text which can fit into 9999 tweets at max.

  * `ty_strom.py` divides every tweet from the input text into exactly 280 chars but also splits the words when reached the limit of 280 chars.


## How to run the program
  * Ensure you have python3 installed on path = `/usr/bin/python3`.
  * Copy the file to desired path using command `cp src/strom.py /opt/hiring/manjit/tweetstorm-generator`.
  * Run the program as `/opt/hiring/yourname/tweetstorm-generator "This is your last chance. After this, there is no turning back. You take the blue pill—the story ends, you wake up in your bed and believe whatever you want to believe. You take the red pill—you stay in Wonderland, and I show you how deep the rabbit hole goes. Remember: all I'm offering is the truth. Nothing more.".`
  * To run the tests for `strom.py` from `/src` directory run `python3 -m unittest test_storm.py`.
